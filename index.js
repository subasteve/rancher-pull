const colors = require('colors');
const dns = require('dns');
const request = require('./Request');

if (process.argv.find((item) => item === "--debug")) {
    process.argv.forEach((item) => {
        console.log('[' + colors.gray("DEBUG") + '][' + colors.cyan("COMMAND") + '][Arg] ' + item);
    });
}

const Command = require('./Command');

if (!Command.token || Command.token === "") {
    console.error("--token REQUIRED");
    process.exit(1);
}

if (!Command.domain || Command.domain === "") {
    console.error("--domain REQUIRED");
    process.exit(1);
}

function lookup(name){
    return new Promise(function(resolve, reject) {
        dns.lookup(name, function (err, result) {
            resolve(result);
        })
    });
}

async function run(){
if (Command.projects) {
    const projects = await request({
        path: '/project/'
    });

    if(!projects){
	return;
    }

    if(projects.type === "error"){
	console.error(projects);
    	return;
    }

    if(!projects.data && !projects.data.length){
    	console.error("Projects data empty!");
    	return;
    }
	
    projects.data.forEach((item) => {
        	if (item.state === "active") {
                    console.log('[' + colors.green("Project") + '][' + colors.gray(item.id) + '] ' + item.name);
                    if (Command.components && item.links) {
                        for (let property in item.links) {
                            if (item.links.hasOwnProperty(property)) {
                                let idx = item.links[property].lastIndexOf("/");

                                if (idx > -1) {
                                    console.log('[' + colors.cyan("Component") + '][' + colors.gray(property) + '] ' + item.links[property].substring(idx));
                                }
                            }
                        }
                    }
                } else {
                    console.log('[' + colors.green("Project") + '][' + colors.yellow(item.id) + '] ' + item.name);
                }
    });

}

if (Command.project) {
    let path = '/project/';

    if(!Command.cluster){
    	if(Command.project.indexOf(":") > -1){
		const split = Command.project.split(":");
		const project = split[1];
		const cluster = split[0];

    		path += Command.project;
		console.log(`[${colors.cyan("Cluster")}][${colors.gray(cluster)}]`);
		console.log(`[${colors.cyan("Project")}][${colors.gray(project)}]`);
    	}else{
		console.error(`[${colors.red("ERROR")}] No Cluster Specified`);
		return;
	}
    }else{
    	path += Command.cluster;
	console.log(`[${colors.cyan("Cluster")}][${colors.gray(Command.cluster)}]`);

	if(Command.project.indexOf(":") > -1){
		const split = Command.project.split(":");
    		const project = split[split.length-1];

		path += `:${project}`;
		console.log(`[${colors.cyan("Project")}][${colors.gray(project)}]`);
    	}else{
		path += `:${Command.project}`;
		console.log(`[${colors.cyan("Project")}][${colors.gray(Command.project)}]`);
	}
    }

    if(Command.namespace){
    	console.log(`[${colors.cyan("Namespace")}][${colors.gray(Command.namespace)}]`);
    }

    if(Command.component){
    	console.log(`[${colors.cyan("Component")}][${colors.gray(Command.component)}]`);
    }

    if(Command.type){
    	console.log(`[${colors.cyan("Type")}][${colors.gray(Command.type)}]`);
    }


    if (Command.component) {
        path += `/${Command.component}/`;

        if (Command.namespace && Command.name) {

            if (Command.type) {
                path += Command.type + ":";
            }

            path += Command.namespace + ":" + Command.name;

	    const body = await request({
        	path: path
    	    });

    	    if(!body){
		return;
    	    }

    	    if(body.type === "error"){
		console.error(body);
    		return;
    	    }

    	    if(!body.containers && !body.containers.length){
    		console.error("Containers empty!");
    		return;
    	    }

	    console.log(`[${colors.green("Resource")}][${colors.gray(Command.namespace)}] ${Command.name}`);

	    if (body && body.containers && body.containers.length > 0 && Command.deploy) {

                        if (!body.containers[0].environment) {
                            body.containers[0].environment = {};
                        }

                        //Modify Enviroment Var
                        let newDate = new Date().toString();
                        let originalDate = body.containers[0].environment.deployment ? body.containers[0].environment.deployment : "";
                        body.containers[0].environment.deployment = newDate;
                        //console.log(body.containers[0].environment);

                        //Make Put with modified data
                        const body2 = await request({
                            path: path,
                            method: "PUT",
                            json: body
                        });

			if(!body2){
				return;
    	    		}

    	    		if(body2.type === "error"){
				console.error(body2);
    				return;
    	    		}

		    	console.log(`Updated Deployment value from ${originalDate} to ${newDate}`);
              }
        } else {
	    const body3 = await request({
                path: path
            });
            
	    if(!body3){
		return;
    	    }

    	    if(body3.type === "error"){
		console.error(body3);
    		return;
    	    }

	    if(!body3.data && !body3.data.length){
    		console.error("Component data empty!");
    		return;
    	    }

            body3.data.forEach(async (item) => {
                        let prefix = `[${colors.cyan(Command.component)}][${colors.gray(item.id)}] `;
                        if (item.state !== "active") {
                            prefix = `[${colors.cyan(Command.component)}][${colors.yellow(item.id)}] `;
                        }
                        if(Command.debug) {
                            console.log(prefix, item);
                        }else if(!Command.check && Command.component != "ingress"){
			    console.log(prefix);
			}
                        if(Command.check && Command.component === "ingress") {
                            if(item.publicEndpoints && item.publicEndpoints.length){
                                item.publicEndpoints.forEach(async (endpoint) => {
                                    if(endpoint) {
                                        let currentIp = await lookup(endpoint.hostname);
                                        if(currentIp === Command.check){
                                            currentIp = colors.cyan("INGRESS");
                                        }else if(currentIp === Command.balancer){
                                            currentIp = colors.cyan("BALANCER");
                                        }else{
                                            currentIp = colors.yellow(currentIp);
                                        }
                                        if(item.tls) {
                                            let cert = item.tls.find((cert) => cert.hosts.indexOf(endpoint.hostname) >= 0);
                                            if (cert) {
                                                endpoint.hostname = colors.green(endpoint.hostname);
                                            } else {
                                                if(endpoint.hostname.indexOf("xip.io") >= 0) {
                                                    endpoint.hostname = colors.yellow(endpoint.hostname);
                                                }else{
                                                    endpoint.hostname = colors.red(endpoint.hostname);
                                                }
                                            }
                                        }else{
                                            if(endpoint.hostname.indexOf("xip.io") >= 0) {
                                                endpoint.hostname = colors.yellow(endpoint.hostname);
                                            }else{
                                                endpoint.hostname = colors.red(endpoint.hostname);
                                            }
                                        }
                                        console.log(`${prefix}[${colors.cyan("DNS")}][${currentIp}] ${endpoint.hostname} ${endpoint.addresses}`);
                                    }
                                });
                            }else{
                                console.log(prefix,colors.red("No publicEndpoints!"));
                            }
                        }
            });
	}
    } else {
        const body4 = await request({
            path: path
        });

	if(!body4){
		return;
    	}

    	if(body4.type === "error"){
		console.error(body4);
    		return;
    	}

	if (body4.state === "active") {
        	console.log(`[${colors.green("Project")}][${colors.gray(body.id)}] ${body4.name}`);
        } else {
        	console.log(`[${colors.green("Project")}][${colors.yellow(body.id)}] ${body4.name}`);
        }
    }
}
}
run();
