const Command = require('./Command');

module.exports = {
	baseUrl: `https://${Command.domain}/v3`
};
