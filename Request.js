const fetch = require('node-fetch');
const colors = require('colors');
const Command = require('./Command');
const Config = require('./Config');

module.exports = async function (requestOptions) {

    if (!requestOptions.method) {
        requestOptions.method = "GET";
    }

    if (Command.debug) {
        console.log('[' + colors.gray("DEBUG") + '][' + colors.cyan("REQUEST") + '][Domain] ' + Command.domain);
        console.log('[' + colors.gray("DEBUG") + '][' + colors.cyan("REQUEST") + '][Path] ' + requestOptions.path);
        console.log('[' + colors.gray("DEBUG") + '][' + colors.cyan("REQUEST") + '][Method] ' + requestOptions.method);
        if (requestOptions.json) {
            console.log('[' + colors.gray("DEBUG") + '][' + colors.cyan("REQUEST") + '][JSON] ', requestOptions.json);
        }
    }

    const options = {
    	headers: {
		'Authorization': 'Bearer '+Command.token,
		'Content-Type': 'application/json'
	},
        method: requestOptions.method
    };

    if(requestOptions.json){
    	options.body = JSON.stringify(requestOptions.json);
    }

    const response = await fetch(Config.baseUrl + requestOptions.path,options);

    const data = await response.json();

    if (Command.debug) {
    	console.log('[' + colors.gray("DEBUG") + '][' + colors.cyan("DATA") + '][JSON]', data);
    }

    return data;
};
