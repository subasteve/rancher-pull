FROM node:lts-alpine

COPY *.js ./
COPY *.json ./
RUN npm install

ENV NPM_CONFIG_LOGLEVEL info
ENV NODE_ENV production

ENTRYPOINT ["node","index.js"]
